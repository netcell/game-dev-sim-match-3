FROM node:6.12.2-alpine

WORKDIR /home/node/game

RUN \
  apk --no-cache add openjdk7 bash git openssh make && \
  npm install -g devkit@3.1.3

COPY --chown=node ./manifest.json ./

RUN devkit install

COPY --chown=node ./package.json ./

RUN npm install && \
  ln -s ./node_modules ./nm && \
  npm cache clean && \
  apk del bash openssh make

CMD [ "devkit", "serve" ]