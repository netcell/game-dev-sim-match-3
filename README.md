# Introduction

This project is run in a docker container. Therefore you would only see the source code folder here: `./src` and `./resources` without `modules` folder. It is because all dependencies would be installed inside the docker container.

You can add npm dependencies in `package.json` file. The dependencies will be automatically installed and symlinked into `src/nm` directory and can be import as:

```
import src.nm.path.to.file as variable
```

# Boot up project

The first time you clone this project you need to build the docker image:

```
docker-compose build
```

Afterward, any time you want to start the project, run:

```
docker-compose up -d
```

And then navigate to https://localhost:9200

When you are done, run the following command to shutdown the container:

```
docker-compose down
```