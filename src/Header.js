import animate;
import ui.View as View;
import ui.ImageView as ImageView;
import ui.resource.Image as Image;
import ui.TextView as TextView;
import src.dimensions as dimensions;

var headerImage = new Image({ url: 'resources/images/ui/header.png'})

exports = Class(ImageView, function(supr) {
  this.init = function(opts) {
    this.opts = merge(opts, {
      y: -headerImage.getHeight(),
      width: headerImage.getWidth(),
      height: headerImage.getHeight(),
      image: headerImage
    })

    supr(this, 'init', [this.opts]);

    this.textView = new TextView({
      superview: this,
      x: 0,
      y: 80,
      width: this.opts.width,
      height: 60,
      size: 24,
      color: '#FFFFFF',
      opacity: 1,
      text: ''
    });

    this.value = {
      number: 0,
      target: 0,
      run: false
    }

    GC.app.engine.on('Tick', bind(this, this.onTick))
  }

  this.show = function() {
    animate(this)
      .now({
        y: 0
      }, 150, animate.easeOut)
      .then({
        y: -20
      }, 150, animate.easeIn)
  }

  this.hide = function() {
    if (this.style.y == -this.opts.height) return;
    animate(this)
      .now({
        y: -20
      }, 150, animate.easeOut)
      .then({
        y: -this.opts.height
      }, 150, animate.easeIn)
  }

  this.onTick = function() {
    var value = this.value;
    if (!value.run) return;
    this.updateText();
    if ((value.number >> 0) == (value.target >> 0)) {
      value.run = false;
    }
  }

  this.setNumber = function(number) {
    if (this.opts.duration) {
      this.value.run = true
      this.value.target = number;
      animate(this.value).now({
        number: number
      }, this.opts.duration, animate.easeInOut)
    } else {
      this.value.number = number;
      this.updateText()
    }
  }

  this.updateText = function() {    
    this.textView.setText(this.opts.prefix + (this.value.number >> 0))
  }
})