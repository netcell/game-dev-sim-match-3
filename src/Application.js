import ui.StackView as StackView;
import src.dimensions as dimensions;
import src.GameScreen as GameScreen;

exports = Class(GC.Application, function () {

  this.initUI = function () {

    var gameScreen = new GameScreen()

    this.rootView = new StackView({
      superview: this,
      x: 0,
      y: 0,
      width: dimensions.width,
      height: dimensions.height,
      clip: true,
      scale: dimensions.scale
    })

    this.rootView.push(gameScreen);

  };

  this.launchUI = function () {

  };

});
