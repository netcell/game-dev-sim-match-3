import ui.resource.Image as Image;
import nm.lodash.lodash as _;

exports.images = [
  'gleam_blue.png',
  'gleam_green.png',
  'gleam_purple.png',
  'gleam_red.png',
  'gleam_white.png',
  'gleam_yellow.png',
  'round_blue.png',
  'round_green.png',
  'round_purple.png',
  'round_red.png',
  'round_white.png',
  'round_yellow.png'
].map(function(name) {
  return new Image({ url: 'resources/images/particles/' + name })
})
