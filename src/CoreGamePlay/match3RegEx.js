import nm.lodash.lodash as _;
import src.CoreGamePlay.gemsType as gemsType;

var types = _.keys(gemsType);

/**
 * UTILITIES
 * =========
 */

/** RegEx that match any number of any characters */
function RegExDistance(number_of_characters) {
  return '.{' + number_of_characters + '}'
}

/** 
 * Find all the matches of pattern over a string, including overlappng matches
 * - pattern: Regular Expression
 * - str: the string to find matches
 * - [Optional] filterFn: Reject matches that the filter returns falsy
 */
function matchAll(pattern, str, filterFn) {
  var results = [], match;
  pattern.lastIndex = 0
  /** Keeps matching until no match */
  while( (match = pattern.exec(str)) != null ) {
    /** Filter the match if needed */
    if ( !filterFn || filterFn(match) ) {
      results.push(match)
    }
    pattern.lastIndex = match.index + 1
  }
  return results;
}

/** 
 * Find the first match of pattern over a string
 * - pattern: Regular Expression
 * - str: the string to find matches
 * - filterFn: Reject matches that the filter returns falsy
 * There are no reason to use this function if you don't have a fileterFn
 */
function matchOne(pattern, str, filterFn) {
  var match;
  pattern.lastIndex = 0
  /** Keeps matching until no match */
  while( (match = pattern.exec(str)) != null ) {
    /** Filter the match */
    if ( filterFn(match) ) {
      return match;
    }
    pattern.lastIndex = match.index + 1
  }
  return null;
}

/**
 * DETECT MATCHES ON A ROW
 * =======================
 */

/** 
 * Build Detect Matches on a Row for an array of gem types
 * ie. `/AAA|BBB|CCC/`
 */
function RegExMatchesRow(types) {
  return new RegExp(
    types.map(
      /** Expression for each type `T` */
      function(T) {
        return [T, T, T].join('');
      }
    ).join('|'),
    'g'
  )
}
/**
 * Returns a filter function to reject matches that have 
 * index less than 3 cells from the end of the row
 * 
 * See Issue 1 in 'Detect Matches on a Row' section at
 * http://bit.ly/2laynF5
 * 
 */
function MatchesRowFilterCreator(number_of_cols) {
  return function(match) {
    /** Reject if no match found */
    if (match == null) return false;
    /** Distance from the match index to end of row */
    var distance_to_EOR = number_of_cols - match.index % number_of_cols;
    /** Rerturn true if match index is more than 2 cells from the end of row */
    return distance_to_EOR > 2;
  }
}
/** Returns the indices of gems to be removed from Matches on Row */
function ExtractIndicesMatchesRow(match) {
  var index = match.index;
  return [index, index + 1, index + 2];
}

/**
 * DETECT MATCHES ON A COLUMN
 * ==========================
 */

/** 
 * Build Detect Matches on a Column for an array of gem types
 * 
 * ie.
 * 0T00
 * 0T00
 * 0T00
 * 
 * /T.{3}T.{3}T/
 */
function RegExMatchesCol(number_of_cols, types) {
  var distance = RegExDistance(number_of_cols - 1);
  
  return new RegExp(
    types.map(
      /** Expression for each type `T` */
      function(T) {
        return [T, distance, T, distance, T].join('');
      }
    ).join('|'),
    'g'
  )
}
/** 
 * Returns a function that returns 
 * the indices of gems to be removed from Matches on Column
 */
function ExtractIndicesMatchesColCreator(number_of_cols) {
  return function(match) {
    var index = match.index;
    return [index, index + number_of_cols, index + 2 * number_of_cols];
  }
}


/**
 * DETECT POTENTIAL MATCHES ON A ROW
 * =================================
 */

/** 
 * Build Detect Potential Matches on a Row for an array of gem types
 * With swappable gem is on the row below
 */
function RegexPotentialMatchesRow(number_of_cols, types) {
  var distance1 = RegExDistance(number_of_cols - 1),
    distance2 = RegExDistance(number_of_cols - 1),
    distance3 = RegExDistance(number_of_cols - 1);
  
  return new RegExp(
    types.map(
      /** Expression for each type `T` */
      function(T) {
        return [
          [ T, T, '.', distance1, T ].join(''),
          [ T, '.', T, distance2, T ].join(''),
          [ '.', T, T, distance3, T ].join('')
        ].join('|')
      }
    ).join('|'),
    'g'
  )
}
/** Reuse MatchesRowFilterCreator for this case */

/** 
 * Build Detect Potential Matches on a Column for an array of gem types
 * With swappable gem is on the row on the right
 */
function RegexPotentialMatchesCol(number_of_cols, types) {
  var distance = RegExDistance(number_of_cols),
    distance1 = RegExDistance(number_of_cols - 1),
    distance2 = RegExDistance(number_of_cols - 1),
    distance3 = RegExDistance(number_of_cols - 1);
  
  return new RegExp(
    types.map(
      /** Expression for each type `T` */
      function(T) {
        return [
          [      T, distance1, T, distance,  T ].join(''),
          [      T, distance,  T, distance2, T ].join(''),
          [ '.', T, distance2, T, distance1, T ].join('')
        ].join('|')
      }
    ).join('|'),
    'g'
  );
}
/**
 * Returns a filter function to reject matches that have 
 * index less than 2 cells from the end of the row
 * 
 * See Issue 1 in 'Detect Matches on a Row' section at
 * http://bit.ly/2laynF5
 * 
 */
function PotentialMatchesColFilterCreator(number_of_cols) {
  return function(match) {
    /** Reject if no match found */
    if (match == null) return false;
    /** Distance from the match index to end of row */
    var distance_to_EOR = number_of_cols - match.index % number_of_cols;
    /** Rerturn true if match index is more than 1 cells from the end of row */
    return distance_to_EOR > 1;
  }
}

exports.types = types;
exports.matchAll = matchAll;
exports.matchOne = matchOne;
exports.RegExMatchesRow = RegExMatchesRow;
exports.MatchesRowFilterCreator = MatchesRowFilterCreator;
exports.RegExMatchesCol = RegExMatchesCol;
exports.RegexPotentialMatchesRow = RegexPotentialMatchesRow;
exports.RegexPotentialMatchesCol = RegexPotentialMatchesCol;
exports.PotentialMatchesColFilterCreator = PotentialMatchesColFilterCreator;
exports.ExtractIndicesMatchesRow = ExtractIndicesMatchesRow;
exports.ExtractIndicesMatchesColCreator = ExtractIndicesMatchesColCreator;
/**
 * Prepare the regular expressions and filters needed for 
 * detecting matches and potential matches in the grid
 */
exports.regExPrepare = function(grid_size) {
  /** Regex for detecting matches */
  this.regExMatchesRow = RegExMatchesRow(types);
  this.regExMatchesCol = RegExMatchesCol(grid_size, types);
  /** Filter for rejecting false matches */
  this.matchesRowFilter = MatchesRowFilterCreator(grid_size);
  /** Regex for detecting potential matches */
  this.regexPotentialMatchesRow = RegexPotentialMatchesRow(grid_size, types);
  this.regexPotentialMatchesCol = RegexPotentialMatchesCol(grid_size, types);
  /** Filter for rejecting false potential matches */
  this.potentialMatchesColFilter = PotentialMatchesColFilterCreator(grid_size);

  this.extractIndicesMatchesRow = ExtractIndicesMatchesRow;
  this.extractIndicesMatchesCol = ExtractIndicesMatchesColCreator(grid_size);

  console.log(
    this.regExMatchesRow,
    this.regExMatchesCol,
    this.regexPotentialMatchesRow,
    this.regexPotentialMatchesCol,
  )
}