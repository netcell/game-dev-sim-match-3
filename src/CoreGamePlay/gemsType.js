import ui.resource.Image as Image;
import nm.lodash.lodash as _;

var types = _.range(6).slice(1);

exports = _.chain(types)
  .mapValues(function(type) {
    return {
      type: type,
      image: new Image({ url: 'resources/images/gems/gem_0' + type + '.png'})
    }
  })
  .keyBy('type')
  .value()