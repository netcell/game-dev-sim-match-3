import nm.lodash.lodash as _;

import ui.View as View;
import ui.ImageView as ImageView;
import ui.resource.Image as Image;

import src.CoreGamePlay.match3RegEx as match3RegEx;
import src.CoreGamePlay.GemCell as GemCell;

var types = match3RegEx.types;
var matchAll = match3RegEx.matchAll;
var matchOne = match3RegEx.matchOne;

/**
 * Required options:
 * - size: the grid is a square: width = height = size
 * - grid_size: No. of rows = No. of cols = grid_size
 * - margin: the size of the gap between two gem, also the size of the gap around the grid.
 */
exports = Class(View, function(supr) {
  this.init = function(opts) {
    this.opts = merge(opts, {
      width: opts.size,
      height: opts.size
    });

    supr(this, 'init', [opts]);

    this.build();

    this.grid_size = opts.grid_size;
    this.number_of_cells = opts.grid_size * opts.grid_size;
    this.gem_size = this.getGemSize();
    /**
     * Prepare the regular expressions and filters needed for 
     * detecting matches and potential matches in the grid
     */
    bind(this, match3RegEx.regExPrepare)(this.grid_size);

    this.grid = [];
  }

  this.build = function() {
    this.on('gems-grid:shuffle', bind(this, function() {
      this.shuffle()
    }))

    this.on('gameover', bind(this, function() {
      this.delAll()
    }))

    this.on('newgame', bind(this, function() {
      this.moves = 7;
      this.score = 0;
      this.emit('update-moves', this.moves);
      this.emit('update-score', this.score);
      this.lockInput();
      this.random()
    }))

    this.makeGestureHanlder()
  }

/**
 * MATCHING MECHANICS
 * ==================
 */

  /** Returns all the row matches */
  this.matchesRow = function() {
    return matchAll(
      this.regExMatchesRow,
      this.toString(),
      this.matchesRowFilter
    )
  }
  /** Returns all the column matches */
  this.matchesCol = function() {
    return matchAll(
      this.regExMatchesCol,
      this.toString()
    )
  }
  /**
   * Return true is the grid has a match
   */
  this.hasAMatch = function() {
    var gridString = this.toString();
    return gridString.match(this.regExMatchesCol) || matchOne(
      this.regExMatchesRow,
      gridString,
      this.matchesRowFilter
    )
  }
  /**
   * Find all matches and convert them into indices of gem cells
   */
  this.getMatchIndices = function() {
    /** Find all matches */
    var matchesRow = this.matchesRow();
    var matchesCol = this.matchesCol();
    /** If no match, returns null rightaway */
    if (!matchesRow.length && !matchesCol.length) {
      return null
    }
    /** 
     * Convert all matches to indices array 
     */
    var matchesIndices = _.uniq(_.flatten(_.concat(
      matchesRow.map(bind(this, this.extractIndicesMatchesRow)),
      matchesCol.map(bind(this, this.extractIndicesMatchesCol)),
    )))
    return matchesIndices;
  }
  /** 
   * Returns true if the provided gridString has
   * - a row potential match with swappable cell in the row below, or
   * - a col potential match with swappable cell in the row to the right
   * This is used to catch potential match by calling this function twice
   * with the grid's string presentation and its reverse
   */
  this.checkPotentialMatches = function(gridString) {
    return matchOne(
      this.regexPotentialMatchesRow,
      gridString,
      this.matchesRowFilter
    ) || matchOne(
      this.regexPotentialMatchesCol,
      gridString,
      this.potentialMatchesColFilter
    );
  }
  /**
   * Return true if the grid has a potential match
   */
  this.hasPotentialMatches = function() {
    var gridString = this.toString();
    var reversedGridString = gridString.split('').reverse().join('');
    return this.checkPotentialMatches(gridString) || this.checkPotentialMatches(reversedGridString);
  }
  /** 
   * Get the string presentation of the grid 
   * Used in finding matches and potential matches
   */
  this.toString = function() {
    return _.map(this.grid, 'type').join('');
  }

/**
 * GENERATION MECHANICS
 * ====================
 */
  /**
   * Create a gem that is attached to this grid
   * Automatically position by index
   * 
   * Event binding should be put here
   */
  this.createGem = function(index, type) {
    var rowcol = this.getRowCol(index)
    var opts = merge(
      this.getGemProp(rowcol.row, rowcol.col), 
      {
        type: type,
      }
    )
    var gem = new GemCell(opts);
    /** Bind envents here */
    return gem;
  }
  /**
   * 
   */
  this.randomGemAt = function(index) {
    var grid = this.grid;
    var gem = grid[index];
    var type = _.sample(types);
    if (gem) {
      gem.setType(type);
    } else {
      gem = grid[index] = this.createGem(index, type)
    }
    return gem;
  }
  /**
   * Fill the grid with random gems so that
   * - There is no mach
   * - There is at least on potential match
   */
  this.random = function() {
    var number_of_cells = this.number_of_cells;
    _.each(
      _.range(number_of_cells),
      bind(this, 'randomGemAt')
    )
    
    /** Retry if conditions not matched */
    if ( this.hasAMatch() || !this.hasPotentialMatches() ) {
      return this.random();
    } else {
      return Promise.all([
        this.grid.map(function(gem) {
          return gem.initAnimPromise
        })
      ]).then(bind(this, 'unlockInput'))
    }
  }
  /**
   * 
   */
  this.fill = function(deletedIndices) {
    return Promise.all([
      this.fall(),
      this.fillEmptyCells(),
    ])
  }
  /**
   * 
   */
  this.fall = function() {
    /** 
     * Iterate all cells from bottom up
     * except for the last row (no where else to fall)
     * 
     */
    return Promise.all(
      _.range(this.number_of_cells - this.grid_size)
        .reverse()
        /** Drop it to the last empty cell below */
        .map(bind(this, 'fallCell'))
    );
  }
  /**
   * 
   */
  this.fallCell = function(index) {
    var rowcol = this.getRowCol(index);
    var col = rowcol.col;    
    /**
     * Find the row of last cell below this cell that is empty
     */
    var target = _.findLast(
      _.range(rowcol.row + 1, this.grid_size),
      bind(this, function(row) {
        return !this.get(row, col);
      })
    );
    /** If the cell exists, swap */
    if (target >= 0) {
      return this.rawSwapCells(rowcol.row, col, target, col, true);
    /** Ignore otherwise */
    } else {
      return Promise.resolve()
    }
    
  }
  /**
   * 
   */
  this.fillEmptyCells = function(empty) {
    empty = empty || _.chain(this.grid)
      .map(function(cell, index) {
        return !cell ? index : null;
      })
      .filter(function(index) {
        return index !== null;
      })
      .value();

    var gems = empty.map(
      bind(this, 'randomGemAt')
    )

    if (!this.hasPotentialMatches()) {
      return this.fillEmptyCells(empty)
    } else {
      return Promise.all(
        gems.map(function(gem) {
          return gem.initAnimPromise;
        })
      )
    }
  }

/**
 * GAME ACTIONS
 * ============
 */
  /**
   * Shuffle the grid to get potential match
   */
  this.shuffle = function() {
    this.lockInput();
    this.grid = _.shuffle(this.grid)
    
    /** Retry if conditions not matched */
    if ( this.hasAMatch() || !this.hasPotentialMatches() ) {
      this.shuffle();
    } else {
      return Promise.all([
        this.grid.map(bind(this, function(gem, index) {
          var pos = this.getGemPropByIndex(index)
          return gem.setPos(pos.x, pos.y);
        }))
      ]).then(bind(this, 'unlockInput'))
    }
  }
  
  /**
   * Swap the gem at (row, col) with the neighbor gem at specified direction
   */
  this.swap = function(row, col, row2, col2) {
    this.lockInput();
    return this.swapWithoutCheck(row, col, row2, col2)
    .then(
      bind(this, 'matchAndDestroy', 0)
    )
    .then(
      /** Swap success, subtract moves by one */
      bind(this, 'decreaseMoves'),
      /**
       * Swap failed, swap back and do nothing
       */
      bind(this, 'swapWithoutCheck', row, col, row2, col2)
    )
    .then(bind(this, 'unlockInput'))
  }

  /**
   * 
   */
  this.swapWithoutCheck = function(row, col, row2, col2) {
    var grid_size = this.grid_size;
    var gem1 = this.get(row, col);
    var gem2 = this.get(row2, col2);
    return this.rawSwapCells(row, col, row2, col2);
  }
  /**
   * Find matches and destroy the gems and refill the grid
   * After each refull, it runs again and counted as a loop
   * `loopCount` indicate the amount of loop, it is used for
   * - If loopCount = 0 then the swap is invalid
   * - We can use loopCount as a combo counter
   */
  this.matchAndDestroy = function(loopCount) {
    /** 
     * Check for matches and destroy if needded 
     * Later if this has animation, we can put it in swapPromise
     */
    var matchIncides = this.getMatchIndices();
    /** If no match found, check the loopCount */
    if (!matchIncides) {
      if (loopCount) {
        return Promise.resolve('No more match found.')
      } else {
        return Promise.reject('Swap invalid: No match found.')
      }
    /** Else, destroy the match gems and refill */
    } else {
      /**
       * ComboCount = LoopCount + 1
       * ie. loop 0 => combo x1, loop 1 => combo x2
       */
      return this.destroyGemsAndRefill(matchIncides, loopCount + 1)
      .then(
        bind(this, 'matchAndDestroy', loopCount + 1)
      )
    }
  }

  this.destroyGemsAndRefill = function(matchIncides, comboCount) {
    this.addScore(matchIncides, comboCount);
    /**
     * Perform destruction
     */
    return Promise.all(
      matchIncides.map(
        bind(this, this.delIndex)
      )
    )
    .then(
      bind(this, function() {
        return this.fill(matchIncides)
      })
    )
  }

/**
 * UTILITIES
 * =========
 */
  this.decreaseMoves = function() {
    this.moves = this.moves - 1;
    this.emit('update-moves', this.moves)
    if (!this.moves) {
      this.emit('gameover')
    }
  }
  /**
   * Calculate the score based on amount of gem matched and comboCount
   */
  this.addScore = function(matchIncides, comboCount) {
    this.score += (_.sum(_.range(matchIncides.length)) + 1) * 10 * comboCount;
    this.emit('update-score', this.score)
  }
  /**
   * Get the index of cell (row, col) in grid array
   */
  this.getIndex = function(row, col) {
    return row * this.grid_size + col;
  }
  /**
   * Get the row and col from index
   */
  this.getRowCol = function(index) {
    var grid_size = this.grid_size;
    return {
      col: index % grid_size,
      row: index / grid_size >> 0
    }
  }
  /**
   * Get the cell at (row, col)
   */
  this.get = function(row, col) {
    return this.grid[this.getIndex(row, col)]
  }
  /**
   * Set the gem for cell at (row, col)
   * Returns the promised returned by GemCell.setPos
   * - [Optional] isFall: if true, will make the fall looks better
   */
  this.set = function(row, col, gem, isFall) {
    this.grid[this.getIndex(row, col)] = gem;
    if (gem) {
      var new_pos = this.getGemProp(row, col);
      return gem.setPos(
        new_pos.x, new_pos.y, null, 
        /** Animations props for falling */
        isFall ? 0.5 * gem.style.x : 0,
        isFall
      );
    } else {
      return Promise.resolve();
    }
  }
  /**
   * remove
   */
  this.del = function(row, col) {
    return this.delIndex(this.getIndex(row, col));
  }
  /**
   * remove
   */
  this.delIndex = function(index) {
    var gem = this.grid[index];
    this.grid[index] = null;
    if (gem) {
      return gem.destroy();
    }
  }
  /**
   * remove all gems
   */
  this.delAll = function() {
    return Promise.all([
      this.grid.map(
        bind(this, function(gem, index) {
          return this.delIndex(index)
        })
      )
    ])
  }
  /**
   * - [Optional] isFall: if true, will use the isFall value
   *   for wait param to make the fall looks better
   */
  this.rawSwapCells = function(row, col, row2, col2, isFall) {
    var gem1 = this.get(row, col),
        gem2 = this.get(row2, col2);
    return Promise.all([
      this.set(row2, col2, gem1, isFall),
      this.set(row, col, gem2, isFall)
    ])
  }
  /**
   * Returns the size of a gem (square) in the grid
   */
  this.getGemSize = function() {
    var opts = this.opts;
    /**
     * The width and height of each gem (square)
     * - Total width of gems = this size - total width margins
     * - Number of margins = number of gems + 1
     */
    var totalWidthOfMargins = (opts.grid_size + 1) * opts.margin;
    return (opts.size - totalWidthOfMargins) / opts.grid_size;
  }
  /**
   * Returns the position of a gem at (col, row)
   * Can be used for animating position when moving gem to a new cell
   */
  this.getGemProp = function(row, col) {
    var opts = this.opts;
    var size = this.gem_size;
    /**
     * Offset from the top left of the grid
     */
    var offsetX = opts.margin + col * (size + opts.margin)
    var offsetY = opts.margin + row * (size + opts.margin)

    return {
      superview: this,
      size: size,
      x: offsetX,
      y: offsetY
    }
  }
  /**
   * Take index in grid string, 
   * find row and col and 
   * get gem position at that cell
   */
  this.getGemPropByIndex = function(index) {
    var grid_size = this.grid_size;
    var rowcol = this.getRowCol(index)
    return this.getGemProp(rowcol.row, rowcol.col)
  }
  /**
   * 
   */
  this.pointToRowCol = function(point) {
    var margin = this.opts.margin;
    var gem_size = this.gem_size;
    var row = point.y / (margin + gem_size) >> 0;
    var col = point.x / (margin + gem_size) >> 0;
    return {
      row: row,
      col: col
    }
  }

/**
 * INPUT HANDLING
 * ==============
 * TODO: Move this into an mixer or subclass
 */
  this.makeGestureHanlder = function() {
    this.inputLock = true;
    /**
     * TODO: Make a subclass for this view and 
     * expose more information on the Swipe event
     * ie. inputStart point
     */
    
    this.on('InputStart', bind(this, this.inputStart));
    this.on('InputSelect', bind(this, this.inputStop));
    this.on('InputMove', bind(this, this.swipe));

    this.currentInputSelection = null;
  }
  /**
   * 
   */
  this.lockInput = function() {
    this.inputLock = true;
  }
  this.unlockInput = function() {
    this.inputLock = false;
  }
  /**
   * On input start, get the relative position of input (point)
   * find the cell it is on and save it's location for later process
   */
  this.inputStart = function(event, point) {
    if (this.inputLock) return this.inputStop();
    this.currentInputSelection = this.pointToRowCol(point);
  }
  /**
   * On input stop, clear the selection
   */
  this.inputStop = function() {
    this.currentInputSelection = null;
  }
  /**
   * On swipe, retrive the selection saved on input start and perform a swap
   */
  this.swipe = function(event, point) {
    if (this.inputLock) return this.inputStop();

    var currentInputSelection = this.currentInputSelection;
    if (!currentInputSelection) {
      return;
    }
    
    var grid_size = this.grid_size;

    var rowcol = this.pointToRowCol(point);
    var row = rowcol.row;
    var col = rowcol.col;

    var row2 = currentInputSelection.row;
    var col2 = currentInputSelection.col;

    row = row2 + Math.sign(row - row2);
    col = col2 + Math.sign(col - col2);

    /**
     * Prevent swapping outside of the grid
     */
    if (row > grid_size - 1 || row < 0 || col > grid_size - 1 || col < 0) {
      return;
    }

    if ((row != row2 && col == col2) || (col != col2 && row == row2) ) {
      /** Make sure we are only swap with a distance of one */
      

      this.swap(row, col, row2, col2)
      this.currentInputSelection = null;
    }
  }
})