import animate;
import nm.lodash.lodash as _;
import ui.ParticleEngine as ParticleEngine;
import src.dimensions as dimensions;
import .particles as particles;
import .gemsType as gemsType;

var images = [
  'gleam_blue.png',
  'gleam_green.png',
  'gleam_purple.png',
  'gleam_red.png',
  'gleam_white.png',
  'gleam_yellow.png',
  'round_blue.png',
  'round_green.png',
  'round_purple.png',
  'round_red.png',
  'round_white.png',
  'round_yellow.png'
].map(function(name) {
  return 'resources/images/particles/' + name
})

/**
 * Required options:
 * - size: the grid is a square: width = height = size
 * - gridSize: No. of rows = No. of cols = gridSize
 * - margin: the size of the gap between two gem, also the size of the gap around the grid.
 */
exports = Class(ParticleEngine, function(supr) {
  this.init = function(opts) {

    /** Set original position to either mid top or mid bottom of the screen */
    supr(this, 'init', [{
      superview: opts.superview,
      width: opts.width,
      height: opts.height,
      centerAnchor: true,
      initCount: 100
    }]);

    // GC.app.engine.on('Tick', bind(this, 'runTick'));
  }

  this.blow = function(gem) {
    // var particleObjects = this.obtainParticleArray(10);

    // var image = gemsType[gem.opts.type].particle;

    // var x = gem.style.x + gem.style.width / 2 - 57/2;
    // var y = gem.style.y + gem.style.height / 2 - 57/2;
    
    // particleObjects.forEach(function(pObj, index) {
    //   pObj.dx = x;
    //   pObj.dy = y;
    //   pObj.polar = true;
    //   pObj.dradius = 400;
    //   pObj.ddradius = -200;
    //   pObj.theta = index * 2 * Math.PI / 10;
    //   // pObj.dx = _.sample([-1, 1]) * _.random(250, 500);
    //   // pObj.dy = _.sample([-1, 1]) * _.random(250, 500);
    //   // pObj.ddy = 350; // gravity is same for all particles
    //   pObj.width = 57;
    //   pObj.height = 57;
    //   pObj.image = image;
    // })

    // this.emitParticles(particleObjects);
  }

})