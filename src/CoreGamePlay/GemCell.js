import animate;
import nm.lodash.lodash as _;
import ui.ImageView as ImageView;
import src.CoreGamePlay.gemsType as gemsType;
import src.CoreGamePlay.match3RegEx as match3RegEx;
import src.dimensions as dimensions;

/**
 * Required options:
 * - size: the grid is a square: width = height = size
 * - gridSize: No. of rows = No. of cols = gridSize
 * - margin: the size of the gap between two gem, also the size of the gap around the grid.
 */
exports = Class(ImageView, function(supr) {
  this.init = function(opts) {

    this.opts = opts;
    
    this.setType(opts.type);

    var superStyle = opts.superview.style

    /** Set original position to either mid top or mid bottom of the screen */
    supr(this, 'init', [{
      superview: opts.superview,
      x: opts.x,
      y: -superStyle.y + -opts.size,
      width: opts.size,
      height: opts.size,
      anchorX: opts.size/2,
      anchorY: opts.size/2,
      canHandleEvents: false
    }]);

    this.openDelay = Math.abs(( superStyle.height - opts.y - opts.size ) + 0.5 * opts.x);

    this.build();
    /** 
     * Animate to specified position with random duration 
     * Random duration provides better feel when all gems come in at once
     */
    this.initAnimPromise = this.setPos(opts.x, opts.y, null, 
      this.openDelay,
      true
    )
  }
  /**
   * Change the type and image of the gem
   */
  this.setType = function(type) {
    this.type = type;
    this.setImage(gemsType[type].image)
  }
  /**
   * Set the position for the gem
   * Automatically animate the transition
   * - x, y is the new position
   * - [Optional] duration=300 is duration of animation
   */
  this.setPos = function(x, y, opts, wait, animateShake) {
    var animation = animate(this)
    var target = {
      x: x, 
      y: y
    }
    var duration = 300;
    var transition = animate.easeIn;
    if (opts) {
      duration   = opts.duration || duration;
      transition = opts.transition || transition;
    }
    if (wait) {
      return this.deplaySetPos(x, y, opts, wait, animateShake);
    } else {
      var anim = animation.now(target, duration, transition)
      if (animateShake) {
        return this.bounce(anim, target)
      } else {
        return anim;
      }
    }
  }
  /**
   * This is just an utility call to deplay the real animation with setTimeout
   * Either I get `animation.wait` wrong or it is really buggy)
   */
  this.deplaySetPos = function(x, y, opts, wait, animateShake) {
    return new Promise(function(resolve, reject) {
      setTimeout(resolve, wait)
    }).then(
      bind(this, function() {
        return this.setPos(x, y, opts, null, animateShake)
      })
    )
  }
  /** 
   * Bounce animation for the bounce after fall
   */
  this.bounce = function(anim, target) {
    return anim
      .then({ y: target.y - 10 }, 50, animate.easeOut)
      .then({ y: target.y }, 50, animate.easeIn)
  }
  /**
   * Destroy animation and remove from superview
   */
  this.destroy = function() {
    return new Promise(bind(this,
      function(resolve, reject) {
        setTimeout(resolve, 0.5 * this.opts.x)
      }
    )).then(bind(this, function() {
      return animate(this)
        .now({
          r: -2 * Math.PI/24,
          scale: 1.1
        }, 100, animate.easeOut)
        .then({
          r: 2 * Math.PI/24,
          scale: 0
        }, 200, animate.easeIn)
    })).then(
      bind(this, 'removeFromSuperview')
    ).catch(function(e) {console.log(e)});
  }
  
  this.build = function() {
    
  }

})