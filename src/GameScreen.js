import nm.lodash.lodash as _;
import ui.View as View;
import ui.ImageView as ImageView;
import ui.resource.Image as Image;
import ui.TextView as TextView;
import .dimensions as dimensions;
import .CoreGamePlay.GemsGrid as GemsGrid;
import .Header as Header;

var backgroundImage = new Image({ url: 'resources/images/ui/background.png'})
var headerImage = new Image({ url: 'resources/images/ui/header.png'})

exports = Class(View, function(supr) {
  this.init = function(opts) {
    opts = merge(opts, {
      x: 0,
      y: 0,
      width: dimensions.width,
      height: dimensions.height
    })

    supr(this, 'init', [opts]);

    this.build();
  }

  this.build = function() {

    this.background = new ImageView({
      superview: this,
      image: backgroundImage,
      x: dimensions.width/2 - backgroundImage.getWidth()/2,
      y: 0,
      width: backgroundImage.getWidth(),
      height: backgroundImage.getHeight()
    })
    /** Anchor gemsGrid to background since the position of the grid is relative to the background.
     */
    this.gemsGrid = new GemsGrid({
      superview: this.background,
      /** Values obtained manually based on the background */
      x: 19,
      y: 307,
      size: 542,
      margin: 15,
      /** May consider making input a square array, which allows holes inside the grid  */
      grid_size: 5
    })

    this.recordHighscore(0)

    this.score = new Header({
      superview: this.background,
      x: 25,
      prefix: 'Score: ',
      duration: 500
    })

    this.gemsGrid.on('newgame', bind(this.score, 'show'))
    this.gemsGrid.on('gameover', bind(this.score, 'hide'))
    this.gemsGrid.on('update-score', bind(this.score, 'setNumber'))
    this.gemsGrid.on('update-score', bind(this, 'recordHighscore'))

    this.moves = new Header({
      superview: this.background,
      x: backgroundImage.getWidth() - 25 - headerImage.getWidth(),
      prefix: 'Moves: '
    })

    this.gemsGrid.on('newgame', bind(this.moves, 'show'))
    this.gemsGrid.on('gameover', bind(this.moves, 'hide'))
    this.gemsGrid.on('update-moves', bind(this.moves, 'setNumber'))

    this.moves.on('InputSelect', bind(this, function () {
			this.gemsGrid.emit('gems-grid:shuffle')
    }));

    new Promise(bind(this,
      function(resolve, reject) {
        setTimeout(resolve, 1000)
      }
    )).then(bind(this.gemsGrid, 'emit', 'newgame'))

    var highScoreView = this.highscoreView = new Header({
      superview: this.background,
      x: backgroundImage.getWidth()/2 - headerImage.getWidth()/2,
      prefix: 'Highscore: ',
      duration: 2000
    })

    this.gemsGrid.on('newgame', bind(this.highscoreView, 'hide'))
    this.gemsGrid.on('gameover', bind(this.highscoreView, 'show'))
    this.gemsGrid.on('gameover', bind(this, function() {
        highScoreView.setNumber(this.highScore)
    }))

    this.newGameView = new TextView({
			superview: this.gemsGrid,
			x: 0,
			y: this.gemsGrid.style.height/2 - 60,
			width: this.gemsGrid.style.width,
			height: 60,
			size: 50,
			color: '#FFFFFF',
      opacity: 1,
      text: 'New Game'
    });

    this.newGameView.hide();

    this.newGameView.on('InputSelect', bind(this, function () {
			this.gemsGrid.emit('newgame');
    }));
    this.gemsGrid.on('newgame', bind(this.newGameView, 'hide'))
    this.gemsGrid.on('gameover', bind(this.newGameView, 'show'))

  }

  this.recordHighscore = function(score) {
    this.highScore = Math.max(this.highScore || 0, score);
  }
})