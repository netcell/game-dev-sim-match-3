import device;

var boundsWidth = 576,
    boundsHeight = 860;

var ratio = boundsWidth/boundsHeight,
    deviceRatio = device.screen.width/device.screen.height;

var baseWidth = boundsWidth,
    baseHeight = device.screen.height * (boundsWidth / device.screen.width);

if (ratio < deviceRatio) {
  baseHeight = boundsHeight,
  baseWidth = device.screen.width * (boundsHeight / device.screen.height);
}

var scale = device.screen.width / baseWidth;

exports = {
  scale: scale,
  width: baseWidth,
  height: baseHeight
}